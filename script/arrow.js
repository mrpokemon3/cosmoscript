export function arrowLeftRotate() {
  //
  //
  const arrowRight = document.querySelectorAll("#arrow-right");
  const arrowLeft = document.querySelectorAll("#arrow-left");

  //
  //

  window.addEventListener("resize", function () {
    if (this.innerWidth < 1920 && this.innerWidth > 1363) {
      let x = (195 + this.innerWidth / 11) * -1;
      arrowRight.forEach(
        (arrow) => (arrow.style.transform = "rotate(" + x + "deg)")
      );
    } else if (this.innerWidth < 1363) {
      arrowRight.forEach(
        (arrow) => (arrow.style.transform = "rotate(" + 320 * -1 + "deg)")
      );
    } else if (this.innerWidth >= 1920) {
      arrowRight.forEach(
        (arrow) => (arrow.style.transform = "rotate(" + 0 + "deg)")
      );
    }
  });
  //
  //
  //
  if (window.innerWidth < 1920 && window.innerWidth > 1526) {
    let q = 250 + window.innerWidth / 10;
    arrowLeft.forEach(
      (arrow) => (arrow.style.transform = "rotate(" + q + "deg)")
    );
  }
  window.addEventListener("resize", function () {
    if (this.innerWidth < 1920 && this.innerWidth > 1526) {
      let x = 250 + this.innerWidth / 10;
      arrowLeft.forEach(
        (arrow) => (arrow.style.transform = "rotate(" + x + "deg)")
      );
    } else if (this.innerWidth >= 1920) {
      arrowLeft.forEach(
        (arrow) => (arrow.style.transform = "rotate(" + 70 + "deg)")
      );
    } else if (this.innerWidth < 1363) {
      arrowLeft.forEach(
        (arrow) => (arrow.style.transform = "rotate(" + 42 + "deg)")
      );
    }
  });
}
