export function addConnect() {
  class Content {
    constructor(img, title, text) {
      this.img = img;
      this.title = title;
      this.text = text;
    }
  }
  const arrContent = [
    new Content(
      "img/planet-connect.png",
      "Приветствие",
      `
    Создадим космический корабль, научимся отправлять и принимать сообщения с земли (Ввод/вывод данных).
    `
    ),
    new Content(
      "img/planet-green.png",
      "Запоминаем имена",
      `
    Научимся управлять кораблем и запишем имя капитана корабля (переменные).`
    ),
    new Content(
      "img/greey-planet.png",
      "Автопилот",
      `
    Научимся облетать астероиды и другие корабли (Условия).`
    ),
    new Content(
      "img/planet-gamepad.png",
      "Автопилот +",
      `
    Усовершенствуем наш Автопилот с помощью цикла (Цикл).`
    ),
    new Content(
      "img/fel-planet.png",
      "Функции",
      `
    Подключим новые модули <br>к нашему кораблю (Функции).
    `
    ),
  ];
  return arrContent;
}
export function renderClass() {
  const arrContent = addConnect();
  class PlanetGenerator {
    arow;
    constructor(buttonBg,arrowId, index, img, title, text, flag) {
      this.arrowId = arrowId;
      this.index = index;
      this.img = img;
      this.title = title;
      this.text = text;
      this.flag = flag;
      this.buttonBg = buttonBg;
    }
    render() {
      const section = document.querySelector(".section-bottom");
      if (this.flag === undefined) {
        this.arow = `
        <img
        class="section-bottom__arrow arrow-default section-bottom__arrow-${this.index}"
        id="${this.arrowId}"
        src="img/arow.png"
        alt=""
      />`;
      } else {
        this.arow = ``;
      }
      section.insertAdjacentHTML(
        "beforeend",
        `
        <div class="section-bottom__wrapper section-bottom__wrapper-${this.index}">
        <div class="section-bottom__img-wrapper">
        ${this.arow}
          <img
            class="section-bottom__img-planet-${this.index}"
            src="${this.img}"
            alt=""
          />
        </div>
        <div class="section-bottom__content section-bottom__content-${this.index}">
          <h2
            class="section-bottom__title-content section-bottom__title-content-${this.index}"
          >
            ${this.title}
          </h2>
          <p class="section-bottom__text section-bottom__text-${this.index}">
            ${this.text}
          </p>
          <button class="section-bottom__button ${this.buttonBg}">
            Бесплатный доступ
          </button>
        </div>
      </div>
      `
      );
    }
  }
  const bottomContentArr = [
    new PlanetGenerator(
      "blue-bg",
      "arrow-right",
      "one",
      arrContent[0].img,
      arrContent[0].title,
      arrContent[0].text
    ),
    new PlanetGenerator(
      "blue-bg",
      "arrow-right",
      "two",
      arrContent[1].img,
      arrContent[1].title,
      arrContent[1].text
    ),
    new PlanetGenerator(
      "orange-bg",
      "arrow-left",
      "tree",
      arrContent[2].img,
      arrContent[2].title,
      arrContent[2].text
    ),
    new PlanetGenerator(
      "orange-bg",
      "arrow-left",
      "four",
      arrContent[3].img,
      arrContent[3].title,
      arrContent[3].text
    ),
    new PlanetGenerator(
      "orange-bg",
      "arrow-left",
      "five",
      arrContent[4].img,
      arrContent[4].title,
      arrContent[4].text,
      true
    ),
  ];
  return bottomContentArr;
}
