export function renderMobileMenu() {
  const arrItems = [
    "Описание игры",
    "Уровни",
    "Тарифы",
    "Отзывы",
    "Партнеры",
    "Профиль",
    "Игра",
  ];

  class RenderMenu {
    trigger;
    sectionTop;
    classNavMobile = "section-top__list";
    constructor(arr) {
      this.arr = arr;
    }
    render() {
      this.sectionTop = document.querySelector(".section-top");
      this.sectionTop.insertAdjacentHTML(
        "afterbegin",
        `
          <nav class="section-top__navigation">
          <div class = "section-top__burger-menu">
            <div class = "section-top__item-burger"></div>
            <div class = "section-top__item-burger"></div>
            <div class = "section-top__item-burger"></div>
          </div>
          <ul class="${this.classNavMobile}">
          </ul>
        </nav>
          `
      );
      this._addItem();
    }
    // Рисует итемы
    _addItem() {
      const list = document.querySelector(`.${this.classNavMobile}`);
      this.arr.forEach((item) => {
        list.insertAdjacentHTML(
          `beforeend`,
          `<li class="section-top__item">${item}</li>`
        );
      });
    }
    clickMenu() {
      const burger = document.querySelector(".section-top__burger-menu");
      const wrapper = document.querySelector(".section-top__navigation");
      const liItem = document.querySelectorAll(".section-top__item")
      const borgerItem = document.querySelectorAll(
        ".section-top__item-burger"
      );
      let flag = true;
      burger.addEventListener("click", function () {
        if (flag === true) {
            liItem.forEach(item=>item.classList.add("section-top__item-active"))
            wrapper.classList.add("section-top__navigation-mobile")
            borgerItem.forEach((item) =>
            item.classList.add("section-top__item-burger-active")
            );
            flag = false;
        } else if (flag === false) {
            liItem.forEach(item=>item.classList.remove("section-top__item-active"))
            wrapper.classList.remove("section-top__navigation-mobile")
          borgerItem.forEach((item) =>
            item.classList.remove("section-top__item-burger-active")
          );
          flag = true;
        }
      });
    }
  }

  return new RenderMenu(arrItems);
}
