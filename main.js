import { arrowLeftRotate } from "./script/arrow.js";
import { renderClass } from "./script/ContentAndRender.js";
import { renderMobileMenu } from "./script/renderMenu.js";
//
//
//

const bottomContentArr = renderClass();
for (let i = 0; i < bottomContentArr.length; i++) {
  bottomContentArr[i].render();
}
arrowLeftRotate();
//
const menu = renderMobileMenu();
menu.render();
menu.clickMenu();
let flag = false;
const btn = document.querySelector(".section-top__button");
btn.addEventListener("click", function () {
  let mb = window.getComputedStyle(btn).marginBottom;
  let mbNew = mb.replace("px", "");
  console.log(mbNew);
  let widthElips = this.clientWidth - 50;
  const defWidth = this.clientWidth;
  if (flag === false) {
    this.style.marginBottom = `${+mbNew + 50}px`;
    this.style.width = `${widthElips}px`;
    this.style.height = `${widthElips}px`;
    flag = true;
  }
  setTimeout(() => {
    this.style.marginBottom = mb;
    this.style.width = `${defWidth}px`;
    this.style.height = `${defWidth}px`;
    flag = false;
  }, 100);
});
